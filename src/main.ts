import { NestFactory } from '@nestjs/core'
import { ConfigService } from '@nestjs/config'
import { AppModule } from './app.module'
// import { PrismaService } from './services/prisma/prisma.service'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const configService = app.get(ConfigService)
  const port = configService.get<number>('port')
  const preBuildEnv = configService.get<string>('preBuildEnv')

  console.info(`app running @ port: `, port)
  console.info(`test binding data from prebuild: `, preBuildEnv)

  // const prismaService: PrismaService = app.get(PrismaService)
  // prismaService.enableShutdownHooks(app)

  await app.listen(port || 3000)
}
bootstrap()
