import { Body, Controller, Post } from '@nestjs/common'
import { User as UserModle } from '@prisma/client'
import { UserService } from './user.service'
import { CreateUserDto } from './dto'

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('')
  async signupUser(@Body() userData: CreateUserDto): Promise<UserModle> {
    return this.userService.createUser(userData)
  }
}
